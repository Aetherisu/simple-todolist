import firebase from 'firebase';


const Firebase = () => {
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyA_z9Yhe59qWLuXKdEmCXZn_6idzvvUncE",
        authDomain: "smart-todo-80a7e.firebaseapp.com",
        databaseURL: "https://smart-todo-80a7e.firebaseio.com",
        projectId: "smart-todo-80a7e",
        storageBucket: "smart-todo-80a7e.appspot.com",
        messagingSenderId: "454667461316",
        appId: "1:454667461316:web:e77dca770f7f011be71f91",
        measurementId: "G-H3WHJ5C8HN"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();

    return firebase.firestore();
}

export default Firebase