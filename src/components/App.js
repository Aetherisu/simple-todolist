import React, { useState } from 'react';
import ItemController from './Items/ItemController'
import './App.css';
import ListController from './Lists/ListController';
import Firebase from './Firebase';

let db = Firebase();

let collectionsRef = db.collection('collections');
let listRef = db.collection('lists').doc('items');

function App() {

  const [page, setPage] = useState("f");
  const [load, setLoad] = useState(false);

  const handlePage = (value) => {
    setPage(value);
    console.log(page);
    setLoad(true);
  }
  const handleLoad = e => {
    setLoad(e);
  }

  return (
    <div className="App">
      {
        load ?
          <ItemController listRef={listRef} handleLoad={handleLoad} collections={page} /> :
          <ListController handleLoad={handleLoad} db={db} listRef={listRef} collectionsRef={collectionsRef} handlePage={handlePage} />
      }
    </div>
  );
}

export default App;
