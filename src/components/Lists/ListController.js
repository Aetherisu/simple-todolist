import React, { useState, useEffect } from 'react';
import HeaderList from '../Headers/HeaderList';
import List from './List';
import _ from 'lodash';


const ListController = (props) => {
    const [collections, setCollections] = useState([]);
    useEffect(() => {
        props.collectionsRef.get()
            .then((snapshot) => {
                snapshot.forEach(doc => {
                    setCollections((prev, id) => {
                        return [
                            ...prev,
                            doc.data(),
                        ]
                    })
                })
            })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    function deleteCollection(db, collectionPath, batchSize) {
        let collectionRef = db.collection('lists').doc('items').collection(collectionPath);
        let query = collectionRef.orderBy('__name__').limit(batchSize);

        return new Promise((resolve, reject) => {
            deleteQueryBatch(db, query, resolve, reject);
        });
    }
    function deleteQueryBatch(db, query, resolve, reject) {
        query.get()
            .then((snapshot) => {
                // When there are no documents left, we are done
                if (snapshot.size === 0) {
                    return 0;
                }

                // Delete documents in a batch
                let batch = db.batch();
                snapshot.docs.forEach((doc) => {
                    batch.delete(doc.ref);
                });

                return batch.commit().then(() => {
                    return snapshot.size;
                });
            }).then((numDeleted) => {
                if (numDeleted === 0) {
                    resolve();
                    return;
                }

                // Recurse on the next process tick, to avoid
                // exploding the stack.
                process.nextTick(() => {
                    deleteQueryBatch(db, query, resolve, reject);
                });
            })
            .catch(reject);
    }

    const handleDelete = (id, title) => {
        setCollections(prev => {
            return prev.filter((data, index) => {
                return index !== id;
            })
        })
        props.collectionsRef.doc(title).delete();
        deleteCollection(props.db, title, 30);
    }
    const handleLists = (value) => {
        setCollections(prev => {
            return [
                ...prev,
                value,
            ]
        });

    }
    return (
        <div>
            <div>
                <HeaderList collection={collections} handleLists={handleLists} />
            </div>
            <div className="main">
                <h2>Your Current To Do Lists</h2>
                {
                    collections.map((data, id) => {
                        const filterTitle = _.capitalize(_.trim(data.title));
                        props.collectionsRef.doc(filterTitle).set(data);
                        return <List
                            collectionsRef={props.collectionsRef}
                            handleDelete={handleDelete}
                            data={data}
                            changePage={props.handlePage}
                            key={id}
                            id={id}
                            list={filterTitle} />
                    })
                }
            </div>
        </div>

    );
}

export default ListController;