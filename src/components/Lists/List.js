import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const List = (props) => {

    const handleClick = e => {
        e.preventDefault();
        props.changePage(props.list);
    }
    const handleDelete = e => {
        e.preventDefault();
        props.handleDelete(props.id, props.list);
    }
    return (
        <div className="list-controller">
            <Card>
                <CardContent>
                    <Typography color="textSecondary" gutterBottom>
                        {props.list}
                    </Typography>
                    <Button onClick={handleClick} color="primary">Open List</Button>
                    <div className="item-delete-btn">
                        <Button onClick={handleDelete} color="secondary">X</Button>
                    </div>
                </CardContent>
            </Card>
            <br />
        </div>

    )
}

export default List;