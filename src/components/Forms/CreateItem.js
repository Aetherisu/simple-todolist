import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import '../Forms/CreateItem.css';

const CreateItem = (props) => {


    const [list, newList] = useState({
        title: "",
        content: ""
    });
    const handleChange = (e) => {
        const { name, value } = e.target;
        newList(prev => {
            return {
                ...prev,
                [name]: value
            }
        });
    }
    const handleClick = (e) => {
        e.preventDefault();
        props.createItem(list);
        newList({
            title: "",
            content: ""
        })
    }
    return (
        <div>
            <form className="form-control">
                <h1>Task Creator</h1>
                <TextField
                    className="input-control"
                    required
                    name="title"
                    onChange={handleChange}
                    placeholder="Title of the task..."
                    id="outlined-required"
                    label="Task Title"
                    value={list.title}
                    variant="outlined"
                />
                <TextField
                    id="outlined-multiline-static"
                    label="Task Description"
                    name="content"
                    onChange={handleChange}
                    className="input-control"
                    placeholder="Description of the task..."
                    multiline
                    rows={4}
                    value={list.content}
                    variant="outlined"
                />
                <Button onClick={handleClick} variant="contained" type="submit" color="primary">
                    Create Task
                </Button>
            </form>
        </div>
    );
}

export default CreateItem;
