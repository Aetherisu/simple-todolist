import React from 'react';
import CreateItem from './CreateItem'

const FormMenu = (props) => {
    return (
        <div className="form-controller">
            <div className="form-container">
                <CreateItem currPage={props.currPage} createItem={props.createItem} id={props.id} />
            </div>
        </div>
    );
}

export default FormMenu;