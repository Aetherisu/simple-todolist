import React from 'react';
import Button from '@material-ui/core/Button';

const HeaderItems = (props) => {
    const handleClick = (e) => {
        props.handleLoad(false);
    }
    return (
        <header className="App-header">
            <div className="header-container">
                <h1>{props.currPage}</h1>
                <Button onClick={handleClick} variant="contained" type="submit" color="primary">Back</Button>
            </div>
        </header>
    );
};

export default HeaderItems;