import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import _ from 'lodash';

const HeaderList = (props) => {
    const [listTitle, setListTitle] = useState({
        title: "",
    });

    const handleChange = e => {
        setListTitle({
            title: e.target.value,
        })

    }
    const handleClick = e => {
        if (props.collection.length !== 0) {
            props.collection.map(data => {
                const titleFilter = _.capitalize(_.trim(listTitle.title));
                if (data.title === titleFilter) {
                    return window.alert("This List Already Exists!");
                }
                else {
                    return props.handleLists(listTitle, props.id);
                }
            })
        } else { props.handleLists(listTitle, props.id); }

        setListTitle({
            title: ""
        })
    }
    return (
        <header className="App-header">
            <div className="header-container">
                {listTitle.title ? <h1>{listTitle.title}</h1> : <h1>Create your list</h1>}
                <TextField
                    className="input-control"
                    required
                    name="To Do List Name"
                    onChange={handleChange}
                    placeholder="Title of the task..."
                    id="outlined-required"
                    label="Task Title"
                    value={listTitle.title}
                    variant="outlined"
                />

            </div>
            <Button onClick={handleClick} variant="contained" type="submit" color="primary">
                Create List
                </Button>
        </header>
    );
};

export default HeaderList;