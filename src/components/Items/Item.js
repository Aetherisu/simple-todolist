import React, { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const Item = (props) => {
    const [state, setState] = useState(false);
    const GreenCheckbox = withStyles({
        root: {
            color: green[400],
            '&$checked': {
                color: green[600],
            },
        },
        checked: {},
    })((props) => <Checkbox color="default" {...props} />);
    const handleChange = (event) => {
        setState(prev => {
            return !prev;
        });
    };
    const handleDelete = (e) => {
        e.preventDefault();
        props.deleteThis(props.id);
    }
    return (
        <div className="item">
            <div className="item-checkbox">
                <FormControlLabel
                    control={<GreenCheckbox checked={state} onChange={handleChange} name="checkedG" />}
                />
            </div>

            <div className="item-content">
                <h2 style={state ? { textDecoration: "line-through", color: "gray" } : { textDecoration: "none" }}>{props.title}</h2>
                <p style={state ? { textDecoration: "line-through", color: "gray" } : { textDecoration: "none" }}> {props.content}</p>
            </div>
            <div className="item-delete-btn">
                <Button onClick={handleDelete} color="secondary">X</Button>
            </div>

        </div>
    );
}

export default Item;