import React, { useEffect, useState } from 'react';
import Item from './Item';
import Fab from '@material-ui/core/Fab';
import FormMenu from '../Forms/FormMenu'
import HeaderItems from '../Headers/HeaderItems';
import Fade from '@material-ui/core/Fade'


const ItemController = (props) => {
    const [menu, setMenu] = useState(false);
    const [listItems, setItems] = useState([]);

    const listRef = props.listRef;
    const collection = props.collections;

    useEffect(() => {
        listRef.collection(collection).get()
            .then((snapshot) => {
                snapshot.forEach(doc => {
                    setItems(prev => {
                        return [
                            ...prev,
                            doc.data(),
                        ]
                    })
                })
            })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    const handleMenu = e => {
        setMenu(prev => {
            return !prev;
        });
    }
    const createNewItem = (data) => {
        setItems((prev, id) => {
            console.log(id);
            return [
                ...prev,
                data
            ]
        })
        setMenu(false);
    };
    const handleDelete = id => {
        setItems(prev => {
            return prev.filter((item, index) => {
                return index !== id;
            })
        })
        props.listRef.collection(props.collections).doc(id.toString()).set(listItems);
    }
    return (
        <div>
            <HeaderItems handleLoad={props.handleLoad} currPage={props.collections} />
            <div className="main">
                {menu ? <Fade in={menu}><FormMenu createItem={createNewItem} /></Fade> : null}
                {
                    listItems.map((data, id) => {
                        props.listRef.collection(props.collections).doc(id.toString()).set(data);
                        return <Item key={id} id={id} deleteThis={handleDelete} title={data.title} content={data.content} />
                    })
                }
                {menu ? <Fab onClick={handleMenu}>-</Fab> : <Fab onClick={handleMenu}>+</Fab>}
            </div>
        </div>

    );
}

export default ItemController;